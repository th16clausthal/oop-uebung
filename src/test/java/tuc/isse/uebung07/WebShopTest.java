package tuc.isse.uebung07;

import static org.junit.Assert.*;

import java.lang.reflect.Array;

import org.junit.Before;
import org.junit.Test;


public class WebShopTest {

	public WebShop shop;
	public Link url;
	public Customer customers;
	
	@Before
	public void setUp() throws Exception {
		shop = new WebShop();
		shop.addItem("Seife", 100);
		shop.addItem("Schokolade", 200);
		shop.addItem("Milch", 130);
		shop.addItem("Butter", 199);
		shop.addItem("Wasser", 50);
		
		customers = new Customer();
		customers.addCustomer("Peter");
		customers.addCustomer("Paul");
	}

	
	@Test
	public void printItem() {
		shop.printItem(3);
	}
	
	@Test
	public void shoppingTest() {
		
		customers.addItemToCart("Peter", 4);
		customers.addItemToCart("Peter", 4);
		customers.addItemToCart("Peter", 3);
		

		
		Kasse kasse = new Kasse();
		
		assert kasse.countCartItem(customers,"Peter") == 3;
		assert kasse.countItems(customers,"Peter",4) == 2;
		assert kasse.countCartItem(customers,"Paul") == 0;
		
		assert kasse.cartSum(shop,customers,"Peter") == 299;
		
		customers.printCustomer();
	}
	
	@Test
	public void testURLs() {
		url = new Link();
		url.addURL("www.shoppingpage.gs");
		url.addURL("www.webshoppingpage.gs");
		url.addURL("www.OnlineShoppingpage.gs");
		url.removeURL("www.webshoppingpage.gs");
		
		assert url.urls.get(0).equals("www.shoppingpage.gs");
		assert url.urls.get(1).equals("www.OnlineShoppingpage.gs");
		
	}
	
	

}
