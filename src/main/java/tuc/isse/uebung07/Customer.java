package tuc.isse.uebung07;

import java.util.List;
import java.util.Vector;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Customer {
	public List<String> customerNames = new Vector<String>(); 	
	public Map<String,List<Integer>> cartItems = new HashMap<String,List<Integer>>();
	// beispiel
	public void addCustomer(String name) {
		customerNames.add(name);
		cartItems.put(name, new Vector<Integer>());		
	}
	
	public void removeCustomer(String name) {
		customerNames.remove(name);
		List<Integer> cart = cartItems.get(name);
		cartItems.remove(cart);
	}
	
	public void printCustomer() {
		for (String name : customerNames) {
			System.out.println("customer:" + name);
		}
	}
	
	public void addItemToCart(String name, int itemIndex) {
		cartItems.get(name).add(itemIndex);
	}

}
