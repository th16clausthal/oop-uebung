package tuc.isse.uebung07;

import java.util.List;


public class Kasse {

	public int cartSum(WebShop shop, Customer customers, String customerName) {
		List<Integer> cart = customers.cartItems.get(customerName);
		Integer sum = 0;
		// test
		for (Integer item : cart) {
			sum += shop.itemPrice.get(item);
		}
		return sum;
	}
	
	public Integer countCartItem(Customer customers, String customerName) {
		List<Integer> cart = customers.cartItems.get(customerName);
		return cart.size();
	}
	
	public Integer countItems(Customer customers, String customerName, Integer itemID) {
		List<Integer> cart = customers.cartItems.get(customerName);
		
		Integer count = 0;
		for (Integer itemInCart : cart) {
			if(itemID==itemInCart) {
				count++;
			}
		}
		return count;
	}

}
